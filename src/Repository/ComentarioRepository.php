<?php

namespace App\Repository;

use App\Entity\Comentario;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Comentario|null find($id, $lockMode = null, $lockVersion = null)
 * @method Comentario|null findOneBy(array $criteria, array $orderBy = null)
 * @method Comentario[]    findAll()
 * @method Comentario[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ComentarioRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Comentario::class);
    }

    public function getAllComments($user)
    {
        $query = $this->getEntityManager()->createQuery('SELECT c.id,c.comentario,post.id from App:Comentario c JOIN c.user user JOIN c.post post where user.id = ?1');
        $query->setParameter(1, $user->getId());

        return $query;
    }

    public function getCommentsFromPost($post)
    {
        $query = $this->getEntityManager()->createQuery('SELECT c.id,c.comentario,post.id,user.email from App:Comentario c JOIN c.user user JOIN c.post post where post.id = ?1');
        $query->setParameter(1, $post);

        return $query;
    }

    // /**
    //  * @return Comentario[] Returns an array of Comentario objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Comentario
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

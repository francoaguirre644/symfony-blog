<?php

namespace App\Repository;

use App\Entity\Post;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Post|null find($id, $lockMode = null, $lockVersion = null)
 * @method Post|null findOneBy(array $criteria, array $orderBy = null)
 * @method Post[]    findAll()
 * @method Post[]    getAllPosts()
 * @method Post[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Post::class);
    }

    public function getAllPostsFromUser($user)
    {
        $query = $this->getEntityManager()->createQuery('SELECT post.id, post.titulo, post.foto, post.fechaPublicacion, user.email from App:Post post JOIN post.user user where user.id = ?1');
        $query->setParameter(1, $user->getId());

        return $query;
    }

    public function getAllPosts()
    {
        $query = $this->getEntityManager()->createQuery('SELECT post.id, post.titulo, post.foto, post.fechaPublicacion, user.email from App:Post post JOIN post.user user');

        return $query;
    }

    // /**
    //  * @return Post[] Returns an array of Post objects
    //  */
    /*public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult();
    }
    */

    /*
    public function findOneBySomeField($value): ?Post
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

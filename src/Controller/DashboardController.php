<?php

namespace App\Controller;

use App\Entity\Comentario;
use App\Entity\Post;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractController
{
    /**
     * @Route("/dashboard", name="dashboard")
     */
    public function index(PaginatorInterface $paginator, Request $request): Response
    {
        $query = $this->getDoctrine()->getManager()->getRepository(Post::class)->getAllPostsFromUser($this->getUser());
        $queryComments = $this->getDoctrine()->getManager()->getRepository(Comentario::class)->getAllComments($this->getUser());

        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            3 /*limit per page*/
        );

        $paginationComments = $paginator->paginate(
            $queryComments, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            3 /*limit per page*/
        );

        return $this->render('dashboard/index.html.twig', [
            'controller_name' => 'HomeController',
            'pagination' => $pagination,
            'paginationComments' => $paginationComments
        ]);
    }
}

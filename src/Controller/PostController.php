<?php

namespace App\Controller;

use App\Entity\Comentario;
use App\Entity\Post;
use App\Form\ComentarioType;
use App\Form\PostType;
use Exception;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;

class PostController extends AbstractController
{

    /**
     * @Route("/posts/new", name="post-new")
     */
    public function create(Request $request, SluggerInterface $slugger): Response
    {
        $post = new Post();

        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $fotoFile = $form->get('foto')->getData();

            // this condition is needed because the 'brochure' field is not required
            // so the PDF file must be processed only when a file is uploaded
            if ($fotoFile) {
                $originalFilename = pathinfo($fotoFile->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = $slugger->slug($originalFilename);
                $newFilename = $safeFilename . '-' . uniqid() . '.' . $fotoFile->guessExtension();

                // Move the file to the directory where brochures are stored
                try {
                    $fotoFile->move(
                        $this->getParameter('fotos_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    throw new Exception('Ha ocurrido un error al intentar subir el archivo');
                }

                // updates the 'brochureFilename' property to store the PDF file name
                // instead of its contents
                $post->setFoto($newFilename);
            }

            $post->setUser($this->getUser());
            $em = $this->getDoctrine()->getManager();
            $em->persist($post);
            $em->flush();
            $this->addFlash('success', Post::REGISTRO_EXITOSO);
            return $this->redirectToRoute('home');
        }

        return $this->render('post/create.html.twig', [
            'controller_name' => 'PostController',
            'formulario' => $form->createView()
        ]);
    }

    /**
     * @Route("/posts/{id}", name="post-view")
     */
    public function viewPost($id, PaginatorInterface $paginator, Request $request): Response
    {
        $comentario = new Comentario();

        $form = $this->createForm(ComentarioType::class, $comentario);
        $form->handleRequest($request);
        $post = $this->getDoctrine()->getRepository(Post::class)->find($id);

        $query = $this->getDoctrine()->getManager()->getRepository(Comentario::class)->getCommentsFromPost($id);

        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            3 /*limit per page*/
        );

        if ($form->isSubmitted() && $form->isValid()) {

            $comentario->setUser($this->getUser());
            $comentario->setPost($this->getDoctrine()->getManager()->getRepository(Post::class)->find($id));
            $em = $this->getDoctrine()->getManager();
            $em->persist($comentario);
            $em->flush();
            $this->addFlash('success', Post::REGISTRO_EXITOSO);
            return $this->redirectToRoute('post-view', array('id' => $id));
        }

        return $this->render('post/post.html.twig', [
            'post' => $post,
            'pagination' => $pagination,
            'formulario' => $form->createView()
        ]);
    }

    /**
     * @Route("/posts/{id}/likes", name="post-like")
     */
    public function likePost($id, Request $request)
    {
        /*
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $post = $em->getRepository(Post::class)->find($id);
        $likes = $post->getLikes();
        $likes .= $user . getId() . ".";
        $post->setLikes($likes);
        $em->flush();

        */
        return $this->json(['likes' => $id]);
    }

    /**
     * @Route("/posts/delete/{id}", name="post-delete")
     */
    public function deletePost($id)
    {

        $em = $this->getDoctrine()->getManager();
        $post = $em->getRepository(Post::class)->find($id);
        $em->remove($post);
        $em->flush();

        $this->addFlash('success', Post::REGISTRO_EXITOSO);

        return $this->redirectToRoute('dashboard');
    }
}

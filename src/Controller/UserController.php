<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    /**
     * @Route("/registro", name="registro")
     */
    public function index(Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $user = new User();

        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $user->setPassword($passwordEncoder->encodePassword(
                $user,
                $form['password']->getData()
            ));
            $em->persist($user);
            $em->flush();
            $this->addFlash('success', User::REGISTRO_EXITOSO);
        }

        return $this->render('user/index.html.twig', [
            'controller_name' => 'UserController',
            'formulario' => $form->createView()
        ]);
    }

    /**
     * @Route("/mi/verme", name="verme")
     */
    public function showUser(): Response
    {
        return $this->render('user/verme.html.twig', [
            'user' => $this->getUser()
        ]);
    }

    /**
     * @Route("/mi/editar", name="editar")
     */
    public function edit(Request $request): Response
    {
        $form = $this->createFormBuilder($this->getUser())
            ->add('nombre', TextType::class)
            ->add('email', EmailType::class)
            ->add('save', SubmitType::class, ['label' => 'Guardar'])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', User::MODIFICACION_EXITOSA);

            return $this->redirectToRoute('verme');
        }

        return $this->render('user/editar.html.twig', [
            'user' => $this->getUser(),
            'formulario' => $form->createView(),
        ]);
    }

    /**
     * @Route("/mi/cambiarpw", name="cambiarPassword")
     */
    public function change_user_password(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $form = $this->createFormBuilder()
            ->add('old_password', PasswordType::class, ['label' => 'Contraseña actual'])
            ->add('new_password', PasswordType::class, ['label' => 'Contraseña'])
            ->add('new_password_confirm', PasswordType::class, ['label' => 'Repetir contraseña'])
            ->add('Actualizar', SubmitType::class, ['label' => 'Actualizar'])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $old_pwd = $form['old_password']->getData();
            $new_pwd = $form['new_password']->getData();
            $new_pwd_confirm = $form['new_password_confirm']->getData();

            $user = $this->getUser();

            $checkPass = $passwordEncoder->isPasswordValid($user, $old_pwd);

            if ($checkPass === true && ($new_pwd_confirm == $new_pwd)) {
                $user->setPassword($passwordEncoder->encodePassword(
                    $user,
                    $new_pwd
                ));

                $this->getDoctrine()->getManager()->persist($user);
                $this->getDoctrine()->getManager()->flush();

                $this->addFlash('success', User::MODIFICACION_EXITOSA);

                return $this->redirectToRoute('verme');
            } else {
                return new jsonresponse(array('error' => 'Bad credentials.'));
            }
        }

        return $this->render('user/editarPass.html.twig', [
            'formulario' => $form->createView()
        ]);
    }
}
